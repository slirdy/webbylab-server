import express from 'express'
import sqlite3 from 'sqlite3'
import bodyParser from 'body-parser'
import cors from 'cors'

const app = express();
const filmDB = new sqlite3.Database("./film.db",
    sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,
    (err) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log('Connected');
            filmDB.run('CREATE TABLE IF NOT EXISTS films (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, release_year INTEGER, format TEXT, stars TEXT, UNIQUE (title))');
        }
    })

app.use(
    cors(),
    bodyParser.json()
);

app.get('/', function (req, res) {
    res.send('Hello!');
});
app.get('/test', function (req, res) {
    res.send(JSON.stringify(req.query));
});

app.get('/film/:id', function (req, res) {
    filmDB.get(`SELECT * FROM films WHERE id=${req.params.id}`, (err, result) => {
        if (err) {
            console.log('ERROR >> ', err)
        } else {
            res.send(JSON.stringify(result));
        }
    })
});
app.delete('/film/:id', function (req, res) {
    filmDB.run(`DELETE FROM films WHERE id=${req.params.id}`, (err, result) => {
        if (err) {
            console.log('ERROR >> ', err)
        } else {
            res.send({ success: true });
        }
    })
});

app.get('/films', function (req, res) {
    if (req.query.find_by && req.query.search) {
        filmDB.all(
            `SELECT id, title 
            FROM films 
            WHERE ${req.query.find_by}
            LIKE  ${"'%" + req.query.search + "%'"}
            ORDER BY ${req.query.sort || 'id'} ${req.query.dir || 'ASC'}`
            , (err, result) => {
                if (err) {
                    console.log('ERROR >> ', err)
                } else {
                    res.send(JSON.stringify(result));
                }
            })
    } else {
        filmDB.all(
            `SELECT id, title
             FROM films 
             ORDER BY ${req.query.sort || 'id'} ${req.query.dir || 'ASC'}`, (err, result) => {
                if (err) {
                    console.log('ERROR >> ', err)
                } else {
                    res.send(JSON.stringify(result));
                }
            })
    }
});

app.post('/film', function (req, res) {
    addFilm(req.body);
    res.send({ success: true })
})

app.post('/films', function (req, res) {
    req.body.forEach(el => {
        addFilm(el);
    })
    res.send({ success: true })
})

// TODO: pagination >> filmDB.get(`SELECT COUNT(*) AS film_length FROM films`, (err, result) => {

function addFilm(film) {
    filmDB.run(`INSERT INTO films 
        (title, release_year, format, stars) 
    VALUES 
        ("${film['title']}", ${film['release_year']}, "${film['format']}", "${film['stars']}")`, function (err) {
            if (err) {
                console.log('ERROR >> ', err)
            }
        })
}

app.listen(3001);